package com.mowitnow.tondeuse;

import com.mowitnow.tondeuse.exceptions.IncorrectLineFormatException;
import com.mowitnow.tondeuse.models.FieldPoint;
import com.mowitnow.tondeuse.models.Orientation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class MachineExecutorTest {

    private InputStream stdin = System.in;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpOutContent() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restoreOutContent() {
        System.setOut(originalOut);
    }

    @Test
    void readFirstLine() throws IncorrectLineFormatException {
        String firstLine = "5 7";

        try {
            System.setIn(new ByteArrayInputStream(firstLine.getBytes()));
            Scanner scanner = new Scanner(System.in);
            MachineExecutor.readFirstLine(scanner);

            assertEquals(5, FieldPoint.getMaxX());
            assertEquals(7, FieldPoint.getMaxY());
        } finally {
            System.setIn(stdin);
        }
    }

    @Test
    void readFirstLine_wrong_format() {
        String firstLine = "56";
        try {
            System.setIn(new ByteArrayInputStream(firstLine.getBytes()));
            Scanner scanner = new Scanner(System.in);
            Exception exception = assertThrows(IncorrectLineFormatException.class, () -> {
                MachineExecutor.readFirstLine(scanner);
            });

            String expectedMessage = "Incorrect first line. Format must be two numbers separated by a space (Ex: 7 9)";
            String thrownMessage = exception.getMessage();
            assertEquals(thrownMessage, expectedMessage);
        } finally {
            System.setIn(stdin);
        }
    }

    @Test
    void readFieldPoint() throws IncorrectLineFormatException {
        FieldPoint fp = MachineExecutor.readFieldPoint("1 2 N", 1);
        assertEquals(new FieldPoint(1, 2, Orientation.NORTH), fp);
    }

    @Test
    void readFieldPoint_wrong_coordinates() {
        Exception exception = assertThrows(IncorrectLineFormatException.class, () -> {
            FieldPoint fp = MachineExecutor.readFieldPoint("A B C", 1);
        });
        String expectedMessage = "Incorrect point coordinates at line 1. Format must be two numbers and a direction separated by a space (Ex: 1 2 N)";
        String thrownMessage = exception.getMessage();
        assertEquals(thrownMessage, expectedMessage);
    }

    @Test
    void readFieldPoint_wrong_orientation() {
        Exception exception = assertThrows(IncorrectLineFormatException.class, () -> {
            MachineExecutor.readFieldPoint("1 5 G", 1);
        });
        String expectedMessage = "Incorrect point orientation at line 1. It must be one of the following direction : N,E,W,S";
        String thrownMessage = exception.getMessage();
        assertEquals(thrownMessage, expectedMessage);
    }

    @Test
    void readCommandsList() throws IncorrectLineFormatException {
        String commands = "AGGD";
        assertArrayEquals(new char[]{'A', 'G', 'G', 'D'}, MachineExecutor.readCommandsList(commands, 1));
    }

    @Test
    void readCommandsList_wrong_commands() {
        String commands = "AGNGD";
        Exception exception = assertThrows(IncorrectLineFormatException.class, () -> {
            MachineExecutor.readCommandsList(commands, 2);
        });
        String expectedMessage = "Incorrect line 2. It must be a list of the following commands : A,G,D. (Ex: AGGDDAAAG)";
        String thrownMessage = exception.getMessage();
        assertEquals(thrownMessage, expectedMessage);
    }

    @Test
    void executeCommands() throws IncorrectLineFormatException {
        FieldPoint.initializeField(5, 5);
        FieldPoint fp = new FieldPoint(1, 2, Orientation.EAST);
        String commands = "AGAD";
        MachineExecutor.executeCommands(fp, commands, 1);
        assertEquals("2 3 E\r\n", outContent.toString());
    }
    @Test
    void executeCommands_throw_exception() {
        String commands = "AG14B";
        Exception exception = assertThrows(IncorrectLineFormatException.class, () -> {
            FieldPoint fp = new FieldPoint(1, 2, Orientation.EAST);
            MachineExecutor.executeCommands(fp, commands, 2);
        });
        String expectedMessage = "Incorrect line 2. It must be a list of the following commands : A,G,D. (Ex: AGGDDAAAG)";
        String thrownMessage = exception.getMessage();
        assertEquals(thrownMessage, expectedMessage);
    }

    @Test
    void executeCommands_stop_at_max_x() throws IncorrectLineFormatException {
        FieldPoint.initializeField(5, 5);
        FieldPoint fp = new FieldPoint(0, 0, Orientation.EAST);
        String commands = "AAAAAAAAAAA";
        MachineExecutor.executeCommands(fp, commands, 1);
        assertEquals("5 0 E\r\n", outContent.toString());
    }
    @Test
    void executeCommands_stop_at_max_Y() throws IncorrectLineFormatException {
        FieldPoint.initializeField(5, 5);
        FieldPoint fp = new FieldPoint(1, 0, Orientation.WEST);
        String commands = "DAAAAAAAAA";
        MachineExecutor.executeCommands(fp, commands, 1);
        assertEquals("1 5 N\r\n", outContent.toString());
    }

    @Test
    void executeCommands_stop_at_boundary_continue_advancing() throws IncorrectLineFormatException {
        FieldPoint.initializeField(5, 5);
        FieldPoint fp = new FieldPoint(1, 1, Orientation.NORTH);
        String commands = "AAAAAAGAADA";
        MachineExecutor.executeCommands(fp, commands, 1);
        assertEquals("0 5 N\r\n", outContent.toString());
    }


    @Test
    void readBody() throws IncorrectLineFormatException {
        FieldPoint.initializeField(5, 5);
        String body = "1 2 N\r\n" +
                "GAGAGAGAA\r\n" +
                "3 3 E\r\n" +
                "AADAADADDA";

        try {
            System.setIn(new ByteArrayInputStream(body.getBytes()));
            Scanner scanner = new Scanner(System.in);
            MachineExecutor.readBody(scanner);
            String expected = "1 3 N\r\n" +
                    "5 1 E\r\n";
            assertEquals(expected, outContent.toString());
        } finally {
            System.setIn(stdin);
        }
    }

    @Test
    void readBody2() throws IncorrectLineFormatException {
        FieldPoint.initializeField(5, 5);
        String body = "1 2 N\r\n" +
                "GAGAGAGAA\r\n" +
                "3 3 E\r\n" ;

        try {
            System.setIn(new ByteArrayInputStream(body.getBytes()));
            Scanner scanner = new Scanner(System.in);
            MachineExecutor.readBody(scanner);
            String expected = "1 3 N\r\n" +
                    "3 3 E\r\n";
            assertEquals(expected, outContent.toString());
        } finally {
            System.setIn(stdin);
        }
    }


}