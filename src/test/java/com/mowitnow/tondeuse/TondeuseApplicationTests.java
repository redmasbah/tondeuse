package com.mowitnow.tondeuse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TondeuseApplicationTests {

    private final PrintStream originalOut = System.out;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeEach
    public void setUpOutContent() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void setOriginalStream() {
        System.setOut(originalOut);
    }

    @Test
    void main_file_not_found() {
        String[] args = new String[]{"ABC.txt"};
        TondeuseApplication.main(args);
        assertTrue(outContent.toString().contains("File not found!"));
    }

    @Test
    void main() {
        String[] args = new String[]{"instructions.txt"};
        TondeuseApplication.main(args);
        String expected = "4 1 S\r\n" +
                "0 4 N\r\n";
        assertEquals(expected, outContent.toString());
    }


}
