package com.mowitnow.tondeuse.exceptions;

public class IncorrectLineFormatException extends Exception {
    public IncorrectLineFormatException(String message) {
        super(message);
    }
}
