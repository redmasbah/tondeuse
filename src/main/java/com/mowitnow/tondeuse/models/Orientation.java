package com.mowitnow.tondeuse.models;

/**
 * Enum of the possible machine orientation
 */
public enum Orientation {
    NORTH('N'),
    EAST('E'),
    WEST('W'),
    SOUTH('S');

    public char code;
    private Orientation left;
    private Orientation right;

    /**
     * Define left and right for each direction
     */
    static {
        NORTH.left = WEST;
        NORTH.right = EAST;
        EAST.left = NORTH;
        EAST.right = SOUTH;
        WEST.left = SOUTH;
        WEST.right = NORTH;
        SOUTH.left = EAST;
        SOUTH.right = WEST;
    }

    Orientation(char code) {
        this.code = code;
    }

    public Orientation getLeft() {
        return left;
    }

    public Orientation getRight() {
        return right;
    }

    public char getCode() {
        return code;
    }

    /**
     * Returns the enum value corresponding to the according code
     * @param code the code of the direction
     * @return the direction
     */
    public static Orientation getFromCode(char code) {
        for (Orientation orientation : Orientation.values()) {
            if (orientation.code == code) {
                return orientation;
            }
        }
        return null;
    }
}
