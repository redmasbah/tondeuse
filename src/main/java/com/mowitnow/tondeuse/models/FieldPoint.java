package com.mowitnow.tondeuse.models;

import java.util.Objects;

import static com.mowitnow.tondeuse.models.Direction.LEFT;

/**
 * Class representing a machine's position and orientation at a given time
 */
public class FieldPoint {
    private int x;
    private int y;
    private Orientation orientation;

    private static int MAX_X;
    private static int MAX_Y;

    public FieldPoint(int x, int y, Orientation orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public static int getMaxX() {
        return MAX_X;
    }

    public static int getMaxY() {
        return MAX_Y;
    }


    /**
     * Static method to initialize field boundaries
     *
     * @param maxX max horizontal coordinate
     * @param maxY max vertical coordinate
     */
    public static void initializeField(int maxX, int maxY) {
        MAX_X = maxX;
        MAX_Y = maxY;
    }

    /**
     * Rotate by 90° left or right
     * @param direction the direction of the rotation
     */
    public void rotate(Direction direction) {
        if(LEFT.equals(direction)) {
            this.orientation = this.orientation.getLeft();
        } else {
            this.orientation = this.orientation.getRight();
        }
    }

    /**
     * Advance by one position following the orientation
     */
    public void advance() {
        switch (this.orientation) {
            case NORTH:
                if (y < MAX_Y) y++;
                break;
            case EAST:
                if (x < MAX_X) x++;
                break;
            case WEST:
                if (x > 0) x--;
                break;
            case SOUTH:
                if (y > 0) y--;
                break;
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldPoint that = (FieldPoint) o;
        return x == that.x &&
                y == that.y &&
                orientation == that.orientation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, orientation);
    }

    @Override
    public String toString() {
        return x + " " + y + " " + orientation.getCode();
    }
}
