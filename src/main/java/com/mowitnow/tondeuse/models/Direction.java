package com.mowitnow.tondeuse.models;

/**
 * Enum of left and right directions
 */
public enum Direction {
    LEFT('G'), RIGHT('D');

    private char code;

    Direction(char code) {
        this.code = code;
    }
    /**
     * Returns the enum value corresponding to the according code
     * @param code the code of the direction
     * @return the direction
     */
    public static Direction getFromCode(char code) {
        for (Direction direction : Direction.values()) {
            if (direction.code == code) {
                return direction;
            }
        }
        return null;
    }

}
