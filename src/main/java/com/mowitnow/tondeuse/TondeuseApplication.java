package com.mowitnow.tondeuse;

import com.mowitnow.tondeuse.exceptions.IncorrectLineFormatException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class TondeuseApplication {
    public static void main(String[] args) {
        File instructionsFile = new File(args[0]);
        try {
            Scanner scanner = new Scanner(instructionsFile);
            MachineExecutor.readAndExecute(scanner);
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            e.printStackTrace();
        } catch (IncorrectLineFormatException e) {
            e.printStackTrace();
        }
    }
}
