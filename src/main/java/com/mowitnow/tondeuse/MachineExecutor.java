package com.mowitnow.tondeuse;

import com.mowitnow.tondeuse.exceptions.IncorrectLineFormatException;
import com.mowitnow.tondeuse.models.Direction;
import com.mowitnow.tondeuse.models.FieldPoint;
import com.mowitnow.tondeuse.models.Orientation;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class containing all methods necessary to execute the machines program
 */
public class MachineExecutor {
    public static final Pattern FIRST_LINE_PATTERN = Pattern.compile("([0-9]+)\\s([0-9]+)");
    public static final Pattern FIELD_POINT_PATTERN = Pattern.compile("([0-9]+)\\s([0-9]+)\\s([a-zA-Z])");
    public static final Pattern COMMANDS_PATTERN = Pattern.compile("[AGD]*");

    /**
     * Read the full list of execution instructions
     * @param scanner Scanner of the file of instructions
     * @throws IncorrectLineFormatException
     */
    public static void readAndExecute(Scanner scanner) throws IncorrectLineFormatException {
        readFirstLine(scanner);
        readBody(scanner);

    }

    /**
     * Read the first line of instructions to establish the boundaries of the field
     * @param scanner Scanner of the file of instructions
     * @throws IncorrectLineFormatException
     */

    static void readFirstLine(Scanner scanner) throws IncorrectLineFormatException {
        if (scanner.hasNextLine()) {
            Matcher firstLineMatcher = FIRST_LINE_PATTERN.matcher(scanner.nextLine());
            if (firstLineMatcher.matches()) {
                FieldPoint.initializeField(Integer.parseInt(firstLineMatcher.group(1)), Integer.parseInt(firstLineMatcher.group(2)));
            } else {
                throw new IncorrectLineFormatException("Incorrect first line. Format must be two numbers separated by a space (Ex: 7 9)");
            }
        }
    }

    /**
     * Read the list of machine positions and the list of commands
     * @param scanner Scanner of the file of instructions
     * @throws IncorrectLineFormatException
     */
    static void readBody(Scanner scanner) throws IncorrectLineFormatException {
        int lineNb = 1;
        while (scanner.hasNextLine()) {
            lineNb++;
            String fieldPointLine = scanner.nextLine();
            FieldPoint fieldPoint = readFieldPoint(fieldPointLine, lineNb);

            if (!scanner.hasNextLine()) {
                System.out.println(fieldPoint);
            } else {
                lineNb++;
                String commands = scanner.nextLine();
                executeCommands(fieldPoint, commands, lineNb);

            }
        }
    }

    /**
     * Execute the list of commands
     * @param fieldPoint the position of the machine
     * @param commands the list of cammands string
     * @param lineNb the line number in the file
     * @throws IncorrectLineFormatException
     */
    static void executeCommands(FieldPoint fieldPoint, String commands, int lineNb) throws IncorrectLineFormatException {
        for (char command : readCommandsList(commands, lineNb)) {
            if(command == 'A'){
                fieldPoint.advance();
            }else {
                fieldPoint.rotate(Direction.getFromCode(command));
            }
        }
        System.out.println(fieldPoint);
    }

    /**
     * Get an array of the commands from the string
     * @param line the commands string
     * @param lineNb the line number in the file
     * @return Commands array
     * @throws IncorrectLineFormatException
     */

    static char[] readCommandsList(String line, int lineNb) throws IncorrectLineFormatException {
        Matcher commandsMatcher = COMMANDS_PATTERN.matcher(line);
        if (!commandsMatcher.matches()) {
            throw new IncorrectLineFormatException("Incorrect line " + lineNb + ". It must be a list of the following commands : A,G,D. (Ex: AGGDDAAAG)");
        }
        return line.toCharArray();
    }

    /**
     * Read the machine's coordinates
     * @param line the coordinates string
     * @param lineNb the line number in the file
     * @return
     * @throws IncorrectLineFormatException
     */
    static FieldPoint readFieldPoint(String line, int lineNb) throws IncorrectLineFormatException {
        Matcher fieldPointMatcher = FIELD_POINT_PATTERN.matcher(line);
        if (fieldPointMatcher.matches()) {
            int x = Integer.parseInt(fieldPointMatcher.group(1));
            int y = Integer.parseInt(fieldPointMatcher.group(2));
            Orientation orientation = Orientation.getFromCode(fieldPointMatcher.group(3).charAt(0));
            if (orientation == null) {
                throw new IncorrectLineFormatException("Incorrect point orientation at line " + lineNb + ". It must be one of the following direction : N,E,W,S");
            }
            return new FieldPoint(x, y, orientation);
        } else {
            throw new IncorrectLineFormatException("Incorrect point coordinates at line " + lineNb + ". Format must be two numbers and a direction separated by a space (Ex: 1 2 N)");
        }
    }
}
