# Tondeuse App

### Running project instructions

* Navigate to the archive directory
* Run `mvn install` to download dependecies (JUnit)
* Run `java com.mowitnow.tondeuse.TondeuseApplication *path_to_file*`
    * Instructions file can be put in the archive directory or referenced by the absolute path

* Unit tests can be run through `mvn test`